package com.example.ciapp.sample2;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("Testuser1", "pAssword");
    }

    @Test

    public void testLoginSuccess() throws LoginFailedException,InvalidPasswordException,UserNotFoundException {

        User user = loginManager.login("Testuser1", "pAssword");
        assertThat(user.getUsername(), is("Testuser1"));
        assertThat(user.getPassword(), is("pAssword"));
    }

    @Test(expected = InvalidPasswordException.class)
    public void testLoginWrongPassword() throws LoginFailedException,InvalidPasswordException,UserNotFoundException {
        User user = loginManager.login("Testuser1", "1A234");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLoginUnregisteredUser() throws LoginFailedException, InvalidPasswordException,UserNotFoundException {
        User user = loginManager.login("inIad", "pAssword");
    }

}